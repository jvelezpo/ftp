use wdpro;

CREATE TABLE IF NOT EXISTS dclship_analytics_data (
    visitorID STRING,
    time_stamp STRING,
    pagename STRING,
    prop1 STRING,
    prop2 STRING,
    prop22 STRING,
    prop42 STRING,
    prop48 STRING,
    events STRING,
    eVar1 STRING,
    eVar10 STRING,
    eVar51 STRING,
    browserWidth STRING,
    browserHeight STRING,
    resolution STRING,
    charSet STRING
)
PARTITIONED BY (voyage_day STRING)
ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
    '/data/WDPRO-CUSTANALYTICS-PROD/archive/DCL/mdxapp/adobe/'
tblproperties ("skip.header.line.count"="1");
