#!/usr/local/bin/python2.7
#-*- coding: utf-8 -*-

from subprocess import check_call
import os
import logging
import sys
import zipfile
import shutil
import ConfigParser
import process_json
import hdfs
import pysftp

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
log.addHandler(ch)

TABLE_NAME = "dclship_analytics_data"
# template used to drop partition in staging table
ADD_PARTITION = "hive -e 'USE wdpro; ALTER TABLE " + TABLE_NAME + " ADD IF NOT EXISTS PARTITION (voyage_day=\"%s\");' "


def loadProperties():
    # reading in variables
    filePath = "/var/dataservices/dataservices.ini"
    log.info("Reading properties in " + filePath)
    header = "Shared"
    config = ConfigParser.ConfigParser()
    config.readfp(open(filePath))

    try:
        tmppath = config.get(header, 'tmppath')
    except ConfigParser.NoOptionError:
        raise Exception(filePath + " must contain an " + header + " section with an item 'tmppath'")
    try:
        hdfspath = config.get(header, 'hdfspath')
    except ConfigParser.NoOptionError:
        raise Exception(filePath + " must contain an " + header + " section with an item 'hdfspath'")
    try:
        basepath = config.get(header, 'basepath')
    except ConfigParser.NoOptionError:
        raise Exception(filePath + " must contain an " + header + " section with an item 'basepath'")

    header = "Adobe_DCL"
    config = ConfigParser.ConfigParser()
    config.readfp(open(filePath))
    try:
        ftp_host = config.get(header, 'dcl_ftp_host')
    except ConfigParser.NoOptionError:
        raise Exception(filePath + " must contain an " + header + " section with an item 'dcl_ftp_host'")
    try:
        ftp_user = config.get(header, 'dcl_ftp_user')
    except ConfigParser.NoOptionError:
        raise Exception(filePath + " must contain an " + header + " section with an item 'dcl_ftp_user'")

    pwPath = "/var/dataservices/dataservices_pw.ini"
    header = "Adobe_DCL"
    config = ConfigParser.ConfigParser()
    config.readfp(open(pwPath))
    try:
        ftp_pwd = config.get(header, 'dcl_ftp_pwd')
    except ConfigParser.NoOptionError:
        raise Exception(filePath + " must contain an " + header + " section with an item 'dcl_ftp_pwd'")

    tmppath = tmppath + "adobe-dcl/"
    hdfspath_output = hdfspath + "DCL/mdxapp/adobe/"
    hdfspath_zip_files = hdfspath + "DCL/mdxapp/zip-files/"
    basepath = basepath + "feed-adobe-dcl/"

    return tmppath, hdfspath_output, hdfspath_zip_files, basepath, ftp_host, ftp_user, ftp_pwd

def deleteLocalDirContent(localDir):
    for f in os.listdir(localDir):
        filePath = os.path.join(localDir, f)
        try:
            if os.path.isfile(filePath):
                os.unlink(filePath)
            elif os.path.isdir(filePath):
                shutil.rmtree(filePath)
        except Exception, e:
            print e.message

def processFiles(file, indir, voyage_day, folder_to_use="WDPRO-AnalyticsDCLMDX-prod"):

    log.info("looking for this folder inside the unzipped file '%s'" % folder_to_use)
    for root, dirs, filenames in os.walk(indir):
        if folder_to_use in root and len(filenames) > 0:
            for f in filenames:
                if f.endswith(".json"):
                    path_to_file = os.path.join(root, f)
                    log.info("process this file '%s'" % path_to_file)

                    lines = process_json.readFile(path_to_file, voyage_day)

                    # Check that the lines coming from the json file has 1 or more lines to write to the output file
                    if len(lines) > 0:
                        log.info("writing '%s' lines to output file" % len(lines))
                        for line in lines:
                            file.write(line)

def createFinalFile(indir, voyage_day_file):

    file_name = os.path.join(indir, voyage_day_file)
    log.info("Create temp output file in '%s'" % file_name)

    f = open(file_name, "w")
    output = u"\t".join(["visitorid", "time_stamp", "pagename", "prop1", "prop2", "prop22", "prop42", "prop48", "events", "evar1", "evar10", "evar51", "browserwidth", "browserheight", "resolution", "charset", "prop35\n"])
    f.write(output)
    return f


def main(argv):
    log.info("Loading properties")
    tmppath, hdfspath_output, hdfspath_zip_files, basepath, ftp_host, ftp_user, ftp_pwd = loadProperties()

    basepath_hive_dir = basepath + "hive/"
    indir = tmppath + "input/"
    log.info("Checking if input dir '%s' exists" % indir)
    if not os.path.exists(indir):
        log.info("Creating input dir '%s'" % indir)
        os.makedirs(indir)
    

    sftp = pysftp.Connection(host=ftp_host, username=ftp_user, password=ftp_pwd)
    ftpfiles = sftp.listdir('/')
    sftp.close()
    for zippedfile in ftpfiles:
        print zippedfile
        sftp = pysftp.Connection(host=ftp_host, username=ftp_user, password=ftp_pwd)
        try:
            sftp.get(zippedfile, indir + zippedfile)
            sftp.close()
        except:
            print "get failed"
            continue

        voyage_day = zippedfile.split(".zip")[0]
        voyage_day_file = "%s.txt" % voyage_day
        hdfspath_output = "%svoyage_day=%s/" % (hdfspath_output, voyage_day)

        path_to_zip_file = os.path.join(indir, zippedfile)
        log.info("zip file to process '%s'" % path_to_zip_file)

        local_tmp_dir = os.path.join(tmppath, voyage_day)
        log.info("unzip it to '%s'" % local_tmp_dir)

        log.info("Checking if local tmp dir '%s' exists" % local_tmp_dir)
        if not os.path.exists(local_tmp_dir):
            log.info("Creating local Tmp dir '%s'" % local_tmp_dir)
            os.makedirs(local_tmp_dir)

        log.info("Deleting content of local tmp dir '%s'" % local_tmp_dir)
        deleteLocalDirContent(local_tmp_dir)

        zip_ref = zipfile.ZipFile(path_to_zip_file, 'r')
        zip_ref.extractall(local_tmp_dir)
        zip_ref.close()

        result_file = createFinalFile(local_tmp_dir, voyage_day_file)
        processFiles(result_file, local_tmp_dir, voyage_day)

        result_file.close()
        log.info("Temp file created at '%s'" % result_file)

        log.info("Check in hdfs if this folder exists '%s'" % hdfspath_output)
        if not hdfs.exists(hdfspath_output):
            log.info("Creating the folder in hdfs '%s'" % hdfspath_output)
            hdfs.mkdir(hdfspath_output)

        final_file_in_hdfs = hdfspath_output + voyage_day_file
        if hdfs.exists(final_file_in_hdfs):
            log.info("Deleting content in hdfs for '%s'" % final_file_in_hdfs)
            hdfs.rm(final_file_in_hdfs)

        # load temp output file to hdfs
        log.info("load local temp file '%s' to hdfs in path '%s'" % (result_file.name, hdfspath_output))
        hdfs.copyFromLocal(result_file.name, hdfspath_output)

        log.info("Deleting content of local tmp dir '%s'" % local_tmp_dir)
        deleteLocalDirContent(local_tmp_dir)

        log.info("Create hive table if doesn't exist '%s'" % TABLE_NAME)
        # create_cmd = "hive -f " + basepath_hive_dir + "create_table.q"
        create_cmd = "hive -f /dclmdx/create_table.q"
        check_call(create_cmd, shell=True)

        log.info("Add partition '%s' to table '%s'" % (voyage_day, TABLE_NAME))
        drop_cmd = ADD_PARTITION % voyage_day
        check_call(drop_cmd, shell=True)

        log.info("Check in hdfs if this folder exists '%s'" % hdfspath_zip_files)
        if not hdfs.exists(hdfspath_zip_files):
            log.info("Creating the folder in hdfs '%s'" % hdfspath_zip_files)
            hdfs.mkdir(hdfspath_zip_files)

        zip_file_in_hdfs = hdfspath_zip_files + f
        if hdfs.exists(zip_file_in_hdfs):
            log.info("Deleting content in hdfs for '%s'" % zip_file_in_hdfs)
            hdfs.rm(zip_file_in_hdfs)

        # load temp output file to hdfs
        log.info("load zip '%s' to hdfs in path '%s'" % (f, hdfspath_zip_files))
        hdfs.copyFromLocal(path_to_zip_file, hdfspath_zip_files)

        log.info("Deleting zip file in local dir '%s'" % path_to_zip_file)
        os.unlink(path_to_zip_file)

if __name__ == '__main__':
    main(sys.argv[1:])
