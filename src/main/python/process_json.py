#!/usr/local/bin/python2.7
#-*- coding: utf-8 -*-

import json
import sys
from datetime import datetime
from pytz import timezone
import pytz
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
log.addHandler(ch)

eventLookup = dict([
    ("pageView", "event3"),
    ("videoStart", "event1"),
    ("assetStart", "event1"),
    ("videoStop", "event11"),
    ("assetComplete", "event11"),
    ("registration", "event4"),
    ("VPDOrder", "event8"),
    ("mapCustomization", "event36"),
    ("recommendationSeen", "event37"),
    ("recommendationSaved", "event38"),
    ("leadGenerated", "event15"),
    ("internalAdClick", "event7"),
    ("internalSearch", "event2"),
    ("bookingSearch", "event13"),
    ("productView", "prodView,event42"),
    ("prodView", "prodView,event42"),
    ("cartCreate", "event61"),
    ("cartView", "scView"),
    ("cartProductAdd", "scAdd,event14"),
    ("cartProductRemove", "scRemove"),
    ("reviewQuote", "event12"),
    ("purchase", "purchase"),
    ("purchaseWithPoints", "event9"),
    ("ticketOrder", "event16"),
    ("activityBooking", "event39"),
    ("cancellation", "event10"),
    ("internalSearchClick", "event5"),
    ("activityCancellation", "event27"),
    ("activityModification", "event28"),
    ("systemProductRemoval", "event29"),
    ("quickQuoteSearch", "event30"),
    ("checkoutDelivery", "event31"),
    ("checkoutInsurance", "event32"),
    ("checkoutApplyPayment", "event33"),
    ("checkoutReviewQuote", "event34"),
    ("benchListViewed", "event35"),
    ("checkoutGuestInfo", "event46"),
    ("guestInfoSubmit", "event46"),
    ("scCheckout", "scCheckout"),
    ("checkout", "scCheckout"),
    ("loggedIn", "event58"),
    ("postCheckoutPayment", "event59"),
    ("failedProduct", "event60"),
    ("selfServiceInitiated", "event48"),
    ("selfServiceCompleted", "event49")
])

def getParam(mapObject, key):
    if key in mapObject:
        return unicode(mapObject[key])
    else:
        return u'Unknown'


def fixDateTime(dt):
    retStr = ""
    try:
        retStr = datetime.fromtimestamp(int(dt)/1000)
    except ValueError:
        retStr = str(dt).replace('T', ' ')
        retStr = datetime.strptime(retStr, '%Y-%m-%d %H:%M:%S.%f')
    return retStr


def fixSelfServiceType(sst):
    if isinstance(sst,list):
        return ",".join(sst)
    else:
        return sst


def readFile(path, voyage_day):

    result = []
    try:
        with open(path.encode('utf-8')) as data_file:
            try:
                data = json.load(data_file)
            except:
                log.info("File is invalid, skipping it")
                return result

        header = data['message']['header']

        if "appInstanceId" in header:

            data_count = len(data['message']['data'])
            log.info("this file has '%s' interactions, going through every one of them to get the final output" % data_count)

            for x in range(0, data_count):
                thedata = data['message']['data'][x]

                # ##
                # Get the necessary values from the json object
                # ##
                # Remove VisitorID invalid characters and limit to 100 bytes
                appInstance_id = unicode(header["appInstanceId"]).replace('-','')[:99]
                application_version = unicode(header["applicationVersion"])
                display_resolution = unicode(header["display"]["resolution"])
                name = header["name"]
                platform = header["platform"]
                platform_version = header["platformVersion"]
                type = getParam(header, "type")
                conversation_id = getParam(thedata, "x-conversation-id")
                connection_type = getParam(thedata, "connectionType")
                date_time = fixDateTime(thedata["dateTime"])
                page_id = getParam(thedata, "pageId").lower()
                self_service_type = fixSelfServiceType(thedata["selfServiceType"])
                site = thedata["site"]

                scEvents = []
                for event in thedata["events"]:
                    if event in eventLookup:
                        scEvents.append(eventLookup[event])
                events = ",".join(scEvents)

                pst_timezone = "PST8PDT"

                time_stamp = date_time.replace(tzinfo = pytz.utc).astimezone(timezone(pst_timezone))

                initial_time = fixDateTime("2008-02-02 00:00:00.000")

                # If this conditions are not meet then go to the next data object
                if page_id.startswith('service') or page_id.startswith('intentservice'):
                    if "1969-" in str(date_time):
                        if time_stamp > initial_time:
                            if date_time <= datetime.utcnow():
                                continue
                # ##
                # Do the last data manipulation before sending the values to the output file
                # ##
                # case when applicationVersion is null then SUBSTR(site,1,100) else concat(SUBSTR(site,1,100),"_",applicationVersion) end  prop1,
                pagename = page_id[0:99]
                if not application_version:
                    prop1 = site[0:99]
                else:
                    prop1 = "%s_%s" % (site[0:99], application_version)
                prop2 = ""
                prop22 = ":".join([type, platform, platform_version, name])[0:99]
                prop42 = conversation_id[0:99]
                prop48 = connection_type[0:99]
                eVar1 = ""
                eVar10 = site[0:244]
                eVar51 = self_service_type[0:244]
                if len(display_resolution.lower().split("x")) == 2:
                    browser_width = display_resolution.lower().split("x")[0]
                    browser_height = display_resolution.lower().split("x")[1]
                else:
                    browser_width = display_resolution.lower()
                    browser_height = display_resolution.lower()
                resolution = display_resolution.lower()
                charSet = "UTF-8"

                # Generate final output line to write on final file
                output = u"\t".join([appInstance_id, time_stamp.strftime("%Y-%m-%d %H:%M:%S"), pagename, prop1, prop2, prop22, prop42, prop48, events, eVar1, eVar10, eVar51, browser_width, browser_height, resolution, charSet, voyage_day, "\n"])
                result.append(output)
        else:
            log.info("this file does not have appInstanceId")
    except:
        print "ERROR"

    return result

def main(argv):
    readFile(argv[0])

if __name__ == '__main__':
    main(sys.argv[1:])
