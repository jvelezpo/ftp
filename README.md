DCL mdxapp adobe
================

##Purpose

Guest use our mobile application on cruise ships.  The analytics from the Guests interactions are recorded then zipped up and sent to us daily.  We take that data, reformat it then send it to Adobe.
Reads the json files and produce the resulting text files for Adobe
They are store on HDFS /data/WDPRO-CUSTANALYTICS-PROD/archive/DCL/mdxapp/adobe/

###How to use
Decompress the tar.gz file and copy the contents to 
/home/WDPRO-CUSTANALYTICS-PROD/services/dcl-mdxapp-adobe/, 
It uses the following properties in /var/dataservices/dataservices.ini:
````java
[Shared]
tmppath: (normally points to /home/WDPRO-CUSTANALYTICS-PROD/tmp/, ending with "/")
hdfspath = (normally points to hdfs://n7cldhnn05.dcloud.starwave.com:9000/data/WDPRO-CUSTANALYTICS-PROD/archive/, ending with "/")
````

Assign execution permissions (if needed):
````java
[user@server]$ chmod u+x main.sh
````

and run the **main.sh** to process a folder hat contains the zip files 
````java
[user@server]$ ./main.sh <path-where-the-zip-files-are>
````

##Distribution

###Using jenkins

The job "dcl-mdxapp-adobe" allows to build a package and deploy the artifact to nexus.

###Manually

In order to create a distribution package from source:

 - Update the version number in pom.xml file: <version>1.0.0-SNAPSHOT</version>
 - run the following maven command
   
```java
[user@server]$ mvn assembly:single
```
This command will generate a tar.gz file under **target/** folder.


###Schedule
* Hourly execution*

###Estimated duration
{*About 1 mins  ~ 2 mins}


###Output
Text file for each zip file inside of <path-where-the-zip-files-are> 
* Archive it to HDFS /data/WDPRO-CUSTANALYTICS-PROD/archive/DCL/mdxapp/adobe/<path-where-the-zip-files-are>/<zip-file-name>.txt

The files that were already in the archived are replaced and the data is overwritten in the archive folder.

###Author
Author name / Author email:

juan sebastian velez / Juan.Sebastian.X.Velez.Posada.-ND@disney.com